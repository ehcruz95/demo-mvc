package git.ehcruz.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DataPosteriorValidator.class)
public @interface DataPosteriorValidation {
	
	String message() default "Data invalida";
	
	Class<?>[] groups() default {};
    
	Class<? extends Payload>[] payload() default {};
}
