package git.ehcruz.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import git.ehcruz.domain.Funcionario;

public class DataPosteriorValidator implements ConstraintValidator<DataPosteriorValidation, Funcionario> {

	@Override
	public boolean isValid(Funcionario obj, ConstraintValidatorContext context) {
		String message = context.getDefaultConstraintMessageTemplate();
		if (obj.getDataSaida() != null && obj.getDataSaida().isBefore(obj.getDataEntrada())) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(message).addPropertyNode("dataSaida").addConstraintViolation();
			return false;
		}
		return true;
	}

}
