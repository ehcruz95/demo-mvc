package git.ehcruz.validator;

import java.time.LocalDate;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import git.ehcruz.domain.Funcionario;

public class FuncionarioValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Funcionario.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Funcionario funcionario = (Funcionario) target;

		LocalDate dataEntrada = funcionario.getDataEntrada();
		LocalDate dataSaida = funcionario.getDataSaida();

		if (dataSaida != null && dataSaida.isBefore(dataEntrada)) {
//			errors.rejectValue("dataSaida", "funcionario.datasaida.posterior");
			errors.rejectValue("dataSaida", "funcionario.datasaida.posterior", "default");
		}
	}

}
