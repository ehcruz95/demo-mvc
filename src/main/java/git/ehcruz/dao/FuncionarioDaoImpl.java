package git.ehcruz.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Repository;

import git.ehcruz.domain.Funcionario;

@Repository
public class FuncionarioDaoImpl extends AbstractDao<Funcionario, Long> implements FuncionarioDao {

	@Override
	public List<Funcionario> findByNome(String nome) {
		String jpql = "FROM Funcionario f WHERE f.nome LIKE concat('%', ?1, '%')";
		return this.createQuery(jpql, nome);
	}

	@Override
	public List<Funcionario> findByIdCargo(Long id) {
		String jpql = "SELECT f FROM Funcionario f JOIN f.cargo as c WHERE c.id = ?1";
		return this.createQuery(jpql, id);
	}

	@Override
	public List<Funcionario> findByDataEntradaSaida(LocalDate dataEntrada, LocalDate dataSaida) {
		String jpql = "FROM Funcionario f WHERE f.dataEntrada >= ?1 AND f.dataSaida <= ?2 ORDER BY f.dataEntrada ASC";
		return this.createQuery(jpql, dataEntrada, dataSaida);
	}

	@Override
	public List<Funcionario> findByDataEntrada(LocalDate dataEntrada) {
		String jpql = "FROM Funcionario f WHERE f.dataEntrada >= ?1 ORDER BY f.dataEntrada ASC";
		return this.createQuery(jpql, dataEntrada);
	}

	@Override
	public List<Funcionario> findByDataSaida(LocalDate dataSaida) {
		String jpql = "FROM Funcionario f WHERE f.dataSaida <= ?1 ORDER BY f.dataEntrada ASC";
		return this.createQuery(jpql, dataSaida);
	}

}
