package git.ehcruz.dao;

import java.util.List;

import git.ehcruz.domain.Cargo;
import git.ehcruz.util.PaginacaoUtil;

public interface CargoDao {

	void save(Cargo departamento);

	void update(Cargo departamento);

	void delete(Long id);

	Cargo findById(Long id);

	List<Cargo> findAll();

	PaginacaoUtil<Cargo> buscaPaginada(int tamanho, int pagina);

}
