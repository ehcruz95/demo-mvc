package git.ehcruz.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import git.ehcruz.domain.Cargo;
import git.ehcruz.util.PaginacaoUtil;

@Repository
public class CargoDaoImpl extends AbstractDao<Cargo, Long> implements CargoDao {

	public PaginacaoUtil<Cargo> buscaPaginada(int tamanho, int pagina) {
		String query = "FROM Cargo c ORDER BY c.nome ASC";
		List<Cargo> cargos = this.getEntityManager().createQuery(query, Cargo.class)
				.setFirstResult((pagina - 1) * tamanho).setMaxResults(tamanho).getResultList();
		long totalPaginas = (this.getCount() + (tamanho - 1)) / tamanho;
		return new PaginacaoUtil<Cargo>(tamanho, pagina, totalPaginas, cargos);
	}

	private long getCount() {
		return this.getEntityManager().createQuery("SELECT count(*) FROM Cargo c", Long.class).getSingleResult();
	}
}
