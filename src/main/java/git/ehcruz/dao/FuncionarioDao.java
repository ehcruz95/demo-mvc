package git.ehcruz.dao;

import java.time.LocalDate;
import java.util.List;

import git.ehcruz.domain.Funcionario;

public interface FuncionarioDao {

	void save(Funcionario departamento);

	void update(Funcionario departamento);

	void delete(Long id);

	Funcionario findById(Long id);

	List<Funcionario> findAll();

	List<Funcionario> findByNome(String nome);

	List<Funcionario> findByIdCargo(Long id);

	List<Funcionario> findByDataEntradaSaida(LocalDate dataEntrada, LocalDate dataSaida);

	List<Funcionario> findByDataEntrada(LocalDate dataEntrada);

	List<Funcionario> findByDataSaida(LocalDate dataSaida);

}
