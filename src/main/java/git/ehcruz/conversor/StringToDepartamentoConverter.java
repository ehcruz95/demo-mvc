package git.ehcruz.conversor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import git.ehcruz.domain.Departamento;
import git.ehcruz.service.DepartamentoService;

@Component
public class StringToDepartamentoConverter implements Converter<String, Departamento> {

	@Autowired
	private DepartamentoService departamentoService;

	@Override
	public Departamento convert(String source) {
		if (source.isEmpty()) {
			return null;
		}
		return this.departamentoService.burcarPorId(Long.valueOf(source));
	}

}
