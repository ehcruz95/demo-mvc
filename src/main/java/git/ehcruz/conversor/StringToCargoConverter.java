package git.ehcruz.conversor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import git.ehcruz.domain.Cargo;
import git.ehcruz.service.CargoService;

@Component
public class StringToCargoConverter implements Converter<String, Cargo> {

	@Autowired
	private CargoService cargoService;
	
	@Override
	public Cargo convert(String source) {
		if(source.isEmpty()) {
			return null;
		}
		return this.cargoService.burcarPorId(Long.valueOf(source));
	}

}
