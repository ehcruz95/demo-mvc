package git.ehcruz.service;

import java.util.List;

import git.ehcruz.domain.Departamento;

public interface DepartamentoService {

	void salvar(Departamento departamento);

	void editar(Departamento departamento);

	void excluir(Long id);

	Departamento burcarPorId(Long id);

	List<Departamento> buscarPorTodos();
	
	Boolean departamentoHasCargo(Long id);
}
