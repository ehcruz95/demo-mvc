package git.ehcruz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import git.ehcruz.dao.DepartamentoDao;
import git.ehcruz.domain.Departamento;

@Service
@Transactional
public class DepartamentoServiceImpl implements DepartamentoService {

	@Autowired
	private DepartamentoDao departamentoDao;

	@Override
	public void salvar(Departamento departamento) {
		this.departamentoDao.save(departamento);
	}

	@Override
	public void editar(Departamento departamento) {
		this.departamentoDao.update(departamento);
	}

	@Override
	public void excluir(Long id) {
		this.departamentoDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Departamento burcarPorId(Long id) {
		return this.departamentoDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Departamento> buscarPorTodos() {
		return this.departamentoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean departamentoHasCargo(Long id) {
		return this.departamentoDao.findById(id).getCargos().isEmpty();
	}

}
