package git.ehcruz.service;

import java.time.LocalDate;
import java.util.List;

import git.ehcruz.domain.Funcionario;

public interface FuncionarioService {

	void salvar(Funcionario funcionario);

	void editar(Funcionario funcionario);

	void excluir(Long id);

	Funcionario burcarPorId(Long id);

	List<Funcionario> buscarPorTodos();

	List<Funcionario> buscarPorNome(String nome);

	List<Funcionario> buscarPorCargo(Long idCargo);

	List<Funcionario> buscarPorData(LocalDate dataEntrada, LocalDate dataSaida);
}
