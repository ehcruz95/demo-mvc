package git.ehcruz.service;

import java.util.List;

import git.ehcruz.domain.Cargo;
import git.ehcruz.util.PaginacaoUtil;

public interface CargoService {

	void salvar(Cargo cargo);
	
	void editar(Cargo cargo);
	
	void excluir(Long id);
	
	Cargo burcarPorId(Long id);
	
	List<Cargo> buscarPorTodos();
	
	Boolean cargoHasFuncionario(Long id);
	
	PaginacaoUtil<Cargo> buscarPorPagina(int tamanho, int pagina);
}
