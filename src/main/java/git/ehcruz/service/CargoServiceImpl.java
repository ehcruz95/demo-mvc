package git.ehcruz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import git.ehcruz.dao.CargoDao;
import git.ehcruz.domain.Cargo;
import git.ehcruz.util.PaginacaoUtil;

@Service
@Transactional(readOnly = false)
public class CargoServiceImpl implements CargoService {

	@Autowired
	private CargoDao cargoDao;

	@Override
	public void salvar(Cargo cargo) {
		this.cargoDao.save(cargo);
	}

	@Override
	public void editar(Cargo cargo) {
		this.cargoDao.update(cargo);
	}

	@Override
	public void excluir(Long id) {
		this.cargoDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Cargo burcarPorId(Long id) {
		return this.cargoDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cargo> buscarPorTodos() {
		return this.cargoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean cargoHasFuncionario(Long id) {
		return this.cargoDao.findById(id).getFuncionarios().isEmpty();
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoUtil<Cargo> buscarPorPagina(int tamanho, int pagina) {
		return this.cargoDao.buscaPaginada(tamanho, pagina);
	}

}
