package git.ehcruz.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import git.ehcruz.dao.FuncionarioDao;
import git.ehcruz.domain.Funcionario;

@Service
@Transactional
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired
	private FuncionarioDao funcionarioDao;

	@Override
	public void salvar(Funcionario funcionario) {
		this.funcionarioDao.save(funcionario);
	}

	@Override
	public void editar(Funcionario funcionario) {
		this.funcionarioDao.update(funcionario);
	}

	@Override
	public void excluir(Long id) {
		this.funcionarioDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Funcionario burcarPorId(Long id) {
		return this.funcionarioDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Funcionario> buscarPorTodos() {
		return this.funcionarioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Funcionario> buscarPorNome(String nome) {
		return this.funcionarioDao.findByNome(nome);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Funcionario> buscarPorCargo(Long idCargo) {
		return this.funcionarioDao.findByIdCargo(idCargo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Funcionario> buscarPorData(LocalDate dataEntrada, LocalDate dataSaida) {
		if (dataEntrada != null && dataSaida != null) {
			return this.funcionarioDao.findByDataEntradaSaida(dataEntrada, dataSaida);
		} else if (dataEntrada != null) {
			return this.funcionarioDao.findByDataEntrada(dataEntrada);
		} else if (dataSaida != null) {
			return this.funcionarioDao.findByDataSaida(dataSaida);
		}
		return new ArrayList<Funcionario>();
	}

}
