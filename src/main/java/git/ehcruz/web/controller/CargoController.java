package git.ehcruz.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import git.ehcruz.domain.Cargo;
import git.ehcruz.domain.Departamento;
import git.ehcruz.service.CargoService;
import git.ehcruz.service.DepartamentoService;
import git.ehcruz.util.PaginacaoUtil;

@Controller
@RequestMapping("/cargos")
public class CargoController {

	@Autowired
	private CargoService cargoService;

	@Autowired
	private DepartamentoService departamentoService;

	@GetMapping("/cadastrar")
	public ModelAndView cadastrar(Cargo cargo, ModelMap model) {
		return new ModelAndView("/cargo/cadastro", model);
	}

	@GetMapping("/listar")
	public String listar(@RequestParam Optional<Integer> tamanho, @RequestParam Optional<Integer> pagina,
			ModelMap model) {
		PaginacaoUtil<Cargo> paginacaoUtil = this.cargoService.buscarPorPagina(tamanho.orElse(5), pagina.orElse(1));
		List<Cargo> cargos = paginacaoUtil.getRegistros();
		model.addAttribute("cargos", cargos);
		model.addAttribute("tamanho", paginacaoUtil.getTamanho());
		model.addAttribute("pagina", paginacaoUtil.getPagina());
		model.addAttribute("totalPaginas", paginacaoUtil.getTotalPaginas());
		return "/cargo/lista";
	}

	@PostMapping("/salvar")
	public String salvar(@Valid Cargo cargo, BindingResult bind, RedirectAttributes redirectAttrs) {
		if (!bind.hasErrors()) {
			this.cargoService.salvar(cargo);
			redirectAttrs.addFlashAttribute("success", "Cargo inserido com sucesso.");
			return "redirect:/cargos/cadastrar";
		}
		return "/cargo/cadastro";
	}

	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Long id, ModelMap model) {
		Cargo cargo = this.cargoService.burcarPorId(id);
		model.addAttribute("cargo", cargo);
		return "/cargo/cadastro";
	}

	@PostMapping("/editar")
	public String editar(@Valid Cargo cargo, BindingResult bind, RedirectAttributes redirectAttrs) {
		if (!bind.hasErrors()) {
			this.cargoService.editar(cargo);
			redirectAttrs.addFlashAttribute("success", "Cargo atualizado com sucesso.");
			return "redirect:/cargos/cadastrar";
		}
		return "/cargo/cadastro";
	}

	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {
		if (!this.cargoService.cargoHasFuncionario(id)) {
			redirectAttrs.addFlashAttribute("fail", "Cargo não removido. Possui funcionario(s) vinculado(s).");
		} else {
			this.cargoService.excluir(id);
			redirectAttrs.addFlashAttribute("success", "Cargo removido com sucesso.");
		}
		return "redirect:/cargos/listar";
	}

	@ModelAttribute("departamentos")
	public List<Departamento> listarDepartamentos() {
		return this.departamentoService.buscarPorTodos();
	}
}
