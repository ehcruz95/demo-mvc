package git.ehcruz.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import git.ehcruz.domain.Departamento;
import git.ehcruz.service.DepartamentoService;

@Controller
@RequestMapping("/departamentos")
public class DepartamentoController {

	@Autowired
	private DepartamentoService departamentoService;

	@GetMapping("/cadastrar")
	public String cadastrar(Departamento departamento) {
		return "/departamento/cadastro";
	}

	@GetMapping("/listar")
	public ModelAndView listar(ModelMap model) {
		List<Departamento> departamentos = this.departamentoService.buscarPorTodos();
		model.addAttribute("departamentos", departamentos);
		return new ModelAndView("/departamento/lista", model);
	}

	@PostMapping("/salvar")
	public String salvar(@Valid Departamento departamento, BindingResult bind, RedirectAttributes redirectAttrs) {
		if (!bind.hasErrors()) {
			this.departamentoService.salvar(departamento);
			redirectAttrs.addFlashAttribute("success", "Departamento adicionado com sucesso.");
			return "redirect:/departamentos/cadastrar";
		}
		return "/departamento/cadastro";
	}

	@GetMapping("/editar/{id}")
	public ModelAndView preEditar(@PathVariable("id") Long id, ModelMap model) {
		Departamento departamento = this.departamentoService.burcarPorId(id);
		model.addAttribute("departamento", departamento);
		return new ModelAndView("/departamento/cadastro", model);
	}

	@PostMapping("/editar")
	public String editar(@Valid Departamento departamento, BindingResult bind, RedirectAttributes redirectAttrs) {
		if (!bind.hasErrors()) {
			this.departamentoService.editar(departamento);
			redirectAttrs.addFlashAttribute("success", "Departamento atualizado com sucesso.");
			return "redirect:/departamentos/cadastrar";
		}
		return "/departamento/cadastro";
	}

	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes redirectAttrs) {
		if (!this.departamentoService.departamentoHasCargo(id)) {
			redirectAttrs.addFlashAttribute("fail", "Departamento não removido. Possui cargo(s) vinculado(s).");
		} else {
			this.departamentoService.excluir(id);
			redirectAttrs.addFlashAttribute("success", "Departamento removido com sucesso.");
		}
		return "redirect:/departamentos/listar";
	}

}
